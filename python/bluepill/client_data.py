# Client data

import sys

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.singleton import Singleton
from bluepill.memory import Memory

CLIENTDATA = "clientData"


class ClientData:
    """
    Persistant client data
    """

    def __init__(self):
        """
        Initialization of the system
        """

        self.homeserver = None
        self.identserver = None
        self.access_token = None
        self.device_id = None
        self.user_id = None
        self.user_name = None
        self.user_avatar_url = None
        self.valid_cert_check = True
        self.encryption = False
        self.rooms = {}
        self.users = {}
        self.roomdir = []
        self.media = {}
        self.room_infos = {}

    def save(self) -> None:
        """
        save myself
        """

        Memory().save_object(CLIENTDATA, self)


class ClientDataFactory(metaclass=Singleton):
    """
    Factory for fetching client data
    """

    def __init__(self):
        """
        Initialization
        """

        self.client_data = None

    @property
    def clientData(self) -> ClientData:
        """
        Get the ClientData object
        """

        if self.client_data:
            return self.client_data

        self.client_data = Memory().get_object(CLIENTDATA)
        if not self.client_data:
            self.client_data = ClientData()

        return self.client_data
