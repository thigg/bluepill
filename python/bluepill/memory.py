"""
Memory factory
"""

PROGNAME = "harbour-bluepill"

import sys
import pyotherside
from pathlib import Path

# from typing import Any, Union
from typing import Any

sys.path.append("../")

from bluepill.singleton import Singleton
from bluepill.cache import Cache
from bluepill.store import Store
from bluepill import util

# import pyotherside


class Memory(metaclass=Singleton):
    """
    Memory class for caching and storing data to disk
    """

    def __init__(self, progname: str = PROGNAME) -> None:
        """
        Initialization
        """

        self._objcache = Cache(limit=50)

        home = Path("~").expanduser()
        xdg_cache_home = home / ".cache"
        util.make_directory(xdg_cache_home)
        self._cache_home = xdg_cache_home / progname
        util.make_directory(self._cache_home)
        self._store = Store(self._cache_home)

    @property
    def cache_home(self) -> Path:
        return self._cache_home

    # def get_object(self, index: str) -> Union[Any, None]:
    def get_object(self, index: str):
        """
        Get an object
        """

        pyotherside.send("get_object", index)

        obj = self._objcache.get(index)
        if not obj:
            obj = self._store.get(index)
            if not obj:
                return None
            else:
                self._objcache.store(index, obj)

        return obj

    def save_object(self, index: str, obj: Any) -> None:
        """
        Save object to disk
        """

        self._store.store(index, obj)

        pyotherside.send("cache_object", index)
        self._objcache.store(index, obj)

    def delete_object(self, index: str) -> None:
        """
        Delete object from disk
        """

        self._store.delete(index)
        self._objcache.delete(index)
