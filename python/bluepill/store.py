"""
Store: a file store for pickling elements
"""

import pickle
import hashlib
from pathlib import Path
import sys
import pyotherside
from typing import Any, Union

sys.path.append("../")


class Store:
    def __init__(self, directory: Path) -> None:
        """
        Initialize the Store.
        Args:
            directory (Path): File directory the date should be stored to
        """

        self.directory = directory

    def _hashme(self, name: str) -> str:
        """
        Create a hash from name
        Args:
            name (str): name to be hashed
        Returns:
            hash to filename.pickle of name
        """

        return hashlib.sha256(name.encode()).hexdigest() + ".pickle"

    def store(self, index: str, element: Any) -> None:
        """
        Store the element in self.directory
        Args:
            index (str): store index name
            element (Any): the element to be pickeled, any type
        """

        fname = self._hashme(index)
        f = self.directory / fname
        with open(f, "wb") as in_handle:
            pickle.dump(element, in_handle)

    def delete(self, index: str) -> None:
        """
        Delete an element file. Send "unknown file" if file not exists
        via pyotherside

        Args:
            index (str): index of the file
        """

        fname = self._hashme(index)
        path = self.directory / fname

        if path.exists():
            path.unlink()
        else:
            pyotherside.send("log", "store delete: unknown file")

    def get(self, index: str) -> Union[Any, None]:
        """
        Get the element from file. Send path found via pyotherside if file
        exists.

        Args:
            index (str): the name of the file
        Returns:
            The element or None
        """

        fname = self._hashme(index)
        path = self.directory / fname

        if path.exists():
            pyotherside.send("log", "store get: path found")
            try:
                with open(path, "rb") as out_handle:
                    return pickle.load(out_handle)
            except:
                return None
        else:
            return None

    def exists(self, index: str) -> bool:
        """
        Check if an index exists as persistent file
        Args:
            index(str): The file index
        Returns:
            True if file exists
        """

        fname = self._hashme(index)
        path = self.directory / fname
        return path.exists()
