# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-bluepill

CONFIG += sailfishapp

SOURCES += src/harbour-bluepill.cpp

DISTFILES += qml/harbour-bluepill.qml \
    qml/components/*.qml \
    qml/cover/*.qml \
    qml/pages/*.qml \
    qml/*.qml \
    qml/pages/FavoritesPage.qml \
    qml/pages/GroupPage.qml \
    qml/pages/LowPrioPage.qml \
    qml/pages/PeoplePage.qml \
    rpm/harbour-bluepill.changes.in \
    rpm/harbour-bluepill.changes.run.in \
    rpm/harbour-bluepill.spec \
    rpm/harbour-bluepill.yaml \
    translations/*.ts \

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-bluepill-de.ts translations/harbour-bluepill-nl.ts

images.path += /usr/share/harbour-bluepill/images
images.files = images/*

python.path = /usr/share/harbour-bluepill/python
python.files = python/*

lib.path = /usr/share/harbour-bluepill/lib
lib.files = build/lib/*

INSTALLS += python images lib
#INSTALLS += python images
