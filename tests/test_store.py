"""
Test bluepill.store
"""

from pathlib import Path
from bluepill.store import Store

import pyotherside

pyoth_args = None


class TestClass(object):
    """
    test class for store
    """

    def test_store_and_get(self, tmpdir, monkeypatch):
        global pyoth_args

        def mockreturn(*argc, **kwargs):
            global pyoth_args
            pyoth_args = argc

        monkeypatch.setattr(pyotherside, "send", mockreturn)

        directory = Path(tmpdir)
        len1 = sum(1 for i in directory.glob("*"))
        store = Store(directory)
        store.store("test1", 1)
        len2 = sum(1 for i in directory.glob("*"))

        assert len2 == len1 + 1
        element = store.get("test1")
        assert pyoth_args[0] == "log"
        assert pyoth_args[1] == "store get: path found"
        assert element == 1

        element2 = store.get("doesnotexist")
        assert not element2

    def test_delete(self, tmpdir, monkeypatch):
        global pyoth_args

        def mockreturn(*argc, **kwargs):
            global pyoth_args
            pyoth_args = argc

        monkeypatch.setattr(pyotherside, "send", mockreturn)

        directory = Path(tmpdir)
        store = Store(directory)
        store.store("test1", 1)
        len1 = sum(1 for i in directory.glob("*"))
        store.delete("test1")
        store.delete("doesnotexist")
        assert pyoth_args[0] == "log"
        assert pyoth_args[1] == "store delete: unknown file"
        len2 = sum(1 for i in directory.glob("*"))

        assert len2 == len1 - 1

    def test_exists(self, tmpdir):
        directory = Path(tmpdir)
        store = Store(directory)
        store.store("test1", 1)
        assert store.exists("test1")
        assert not store.exists("doesnotexist")
