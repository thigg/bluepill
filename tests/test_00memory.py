"""
Test memory cache
"""

# from bluepill.memory import Memory

from pathlib import Path, PosixPath
import pathlib
from bluepill.memory import Memory
import pyotherside


class FakeObject(object):
    def __init__(self, testvar):
        self.testvar = testvar


class TestClass(object):
    def test_cache_home(self, tmpdir, monkeypatch):
        def mockreturn(path):
            return Path(tmpdir)

        progname = "testmemory"

        monkeypatch.setattr(
            pathlib.PosixPath, "expanduser", mockreturn, raising=True
        )

        m = Memory(progname)
        cachedir = Path(tmpdir) / ".cache"
        assert cachedir.exists()
        ttmp = cachedir / progname
        assert type(m) == Memory
        home = m.cache_home
        assert type(home) == PosixPath
        assert home == ttmp
        assert home.exists()

    def test_save_load_delete(self, tmpdir, monkeypatch):
        def mockreturn(path):
            return Path(tmpdir)

        progname = "testmemory"

        monkeypatch.setattr(
            pathlib.PosixPath, "expanduser", mockreturn, raising=True
        )

        global pyoth_args

        def mockreturn2(*argc, **kwargs):
            global pyoth_args
            pyoth_args = argc

        monkeypatch.setattr(pyotherside, "send", mockreturn2)

        m = Memory(progname)

        fobject = FakeObject(1)
        assert type(fobject) == FakeObject

        m.save_object("testobject", fobject)
        assert pyoth_args[0] == "cache_object"

        fobject2 = m.get_object("testobject")
        assert pyoth_args[0] == "get_object"
        assert not m.get_object("notexist")

        assert type(fobject2) == FakeObject
        assert fobject2.testvar == 1

        m.delete_object("testobject")
        assert not m.get_object("testobject")
