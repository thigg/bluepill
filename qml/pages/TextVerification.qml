import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    id: thedialog

    property string user_id
    property string device_id
    property string display_name
    property string thekey
    property int timeoutval: 30

    Component.onCompleted: {
        clienthandler.getUserProfile(user_id)
    }

    onTimeoutvalChanged: {
        if (timeoutval == 0) {
            thedialog.reject()
        }
    }

    Column {
        width: parent.width
        anchors.margins: Theme.paddingMedium

        DialogHeader { title: qsTr('Verify device')}

        Item {
              Timer {
                  interval: 1000; running: true; repeat: true
                  onTriggered: timeoutval -= 1
              }
          }

        ProgressBar {
            id: thetimeout
            width: parent.width
            height: Theme.itemSizeSmall
            minimumValue: 0
            maximumValue: 30
            value: timeoutval
        }

        Label {
            text: qsTr('Device ID')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: thedeviceid
            text: device_id
        }

        Label {
            text: qsTr('Device Name')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: thename
            text: display_name
        }

        Label {
            text: qsTr('User ID')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            wrapMode: Text.WordWrap
            padding: Theme.paddingMedium
            id: theuserid
            text: user_id
        }

        Label {
            text: qsTr('Session Key')
            color: Theme.highlightColor
            padding: Theme.paddingMedium
        }

        Label {
            width: parent.width
            height: contentHeight
            padding: Theme.paddingMedium
            wrapMode: Text.WordWrap
            text: thekey.replace(/(.{4})/g,"$1 ")
        }
    }
}
