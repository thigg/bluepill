import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property string room_id
    property string room_name
    property bool renderflag: true
    property bool buckstart: true
    property string bucketid: ""
    property var usersinfo
    property var lu: " "
    property var fu: " "

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        id: eventlist
        anchors.fill: parent
        width: parent.width
        height: contentHeight
        quickScroll: true
        snapMode: ListView.SnapOneItem
        flickDeceleration: 500

        property string lastuser: ""
        property string lname: room_name
        property bool autoLoadMore: true
        property bool loadStarted: false

        function loadData(mode) {
            if (mode === "prepend") {
                // clienthandler.getBucketFrom(room_id, bucketid)
            }
        }

        function positionViewAtEnd() {

        }

        header: Item {
            visible: eventlist.autoLoadMore
            width: parent.width
            height: Theme.itemSizeLarge
            Button {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    margins: Theme.paddingSmall
                    bottomMargin: Theme.paddingLarge
                }
                visible: false
                onClicked: {
                    loadData("prepend")
                }
            }
            BusyIndicator {
                size: BusyIndicatorSize.Small
                running: eventlist.loadStarted
                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
            }
        }

        footer: TextControl {
            id: textcontrol
            visible: !renderflag
            user_id: "" // muser_id
            avatar_url: "" // mavatar_url
            lname: "" // mlname
            room_id: room_id
        }


        PushUpMenu {
            MenuItem {
                text: qsTr('Preferences: ') + room_name
            }

            MenuItem {
                text: qsTr("Preferences")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"))
            }
        }

        Component.onCompleted: {
            // renderflag = true
            clienthandler.getRoomInfo(room_id)
        }

        onContentYChanged: {
            if (contentY + height > headerItem.y && !eventlist.loadStarted && eventlist.autoLoadMore) {
                loadData("prepend")
                eventlist.loadStarted = true
            }
        }

        Connections {
            target: clienthandler
            onRoomInfo: {
                usersinfo = users
                console.log("entering room: get hot bucket", room_id)
                eventlist.positionViewAtEnd()
                clienthandler.getBucket(room_id, 'hot_bucket')
                buckstart = true
            }

            onBucketGet: {
                bucketid = bucket_id
                console.log('bucket got', bucket_id, bucket.forward)
                if (bucket.forward===true) {
                    var i
                    var ll = " "

                    for (i=bucket.events.length - 1; i >= 0; --i) {
                        if(i != 0) {
                            lu = bucket.events[i - 1].sender
                        }
                        fu = bucket.events[i].sender
                        if (ll === "") {
                            ll = bucket.events[i].sender
                        }
                        if (i >= 1) {
                            bucket.events[i].lastuser = lu
                        } else {
                            bucket.events[i].lastuser = " "
                        }

                        eventsModel.insert(0, bucket.events[i])
                        if (buckstart) {
                            eventlist.scrollToBottom()
                        }
                        lu = bucket.events[i].sender
                    }
                    lu = ll
                } else {
                    for (i = 0; i < bucket.events.length - 1; i++) {
                        if (i !==  bucket.events.length) {
                            eventlist.lastuser = bucket.events[i + 1].sender
                        } else {
                            eventlist.lastuser = ""
                        }
                        eventsModel.insert(0, bucket.events[i])
                        if (buckstart) {
                            eventlist.scrollToBottom()
                        }
                    }
                }

                eventlist.loadStarted = false
                // eventlist.scrollToBottom()
            }


            onRoomMessageText: {
                if (room_id == room) {
                    console.log("new message in room", room_id)
                    eventlist.positionViewAtEnd()
                    event.lastuser = lu
                    eventsModel.append(event)
                    lu = event.sender
                }
            }
        }


//            onRoomEvents: {
//                console.log("in onRoomEvents")
//                renderflag = false
//                eventsModel.clear()
//                for (var i=0; i < events.length; i++) {
//                    if (eventlist.lastuser !== events[i].name || events[i].image !== "") {
//                        eventlist.lname = events[i].name
//                        eventlist.lastuser = eventlist.lname
//                    } else {
//                        eventlist.lname = ""
//                    }

//                    events[i].lname = eventlist.lname
//                    eventsModel.append(events[i])
//                }
//                if(pvae) {
//                    eventlist.positionViewAtEnd()
//                }
//                clienthandler.getRoomMembers(room_id)
//            }
//            onRoomMembers: {
//                console.log("room members")
//            }

//            onRoomMessageText: {
//                console.log("RoomMessageText", room.room_id, page.room_id)
//                if (room.room_id !== page.room_id) {
//                    return
//                }

//                if (eventlist.lastuser !== ename) {
//                    event.lname = ename
//                    eventlist.lname = ename
//                    eventlist.lastuser = eventlist.lname
//                } else {
//                    event.lname = ""
//                }

//                eventsModel.append(event)
//                eventlist.positionViewAtEnd()
//                clienthandler.seenMessage(room_id)
//            }
//            onUserInfo: {
//                console.log("onUserInfo")
//                mlname = userdata.lname
//                mavatar_url = userdata.avatar_url
//                muser_id = userdata.user_id
//            }
//        }

//        ViewPlaceholder {
//            id: placeholder
//            enabled: renderflag
//            text: qsTr("Rendering")
//            hintText: qsTr("Collecting Posts")
//        }
//        section.property: 'asect'
//        section.delegate: SectionHeader {
//            text: section
//            horizontalAlignment: Text.AlignRight
//            font.bold: true
//        }

        model: ListModel {
            id: eventsModel
            property string lastuser: ""
        }
        delegate: EventsListItem { }
    }
}
