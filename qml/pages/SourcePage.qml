import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    property string eventsource
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        Component.onCompleted: {
            console.log(page.eventsource)
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: parent.height

        Column {
            id: sourcecolumn
            anchors.fill: parent
            width: parent.width
            clip: true
            spacing: Theme.paddingMedium
            Label {
                id: sourceLabel
                width: parent.width
                padding: Theme.paddingMedium
                wrapMode: Text.WordWrap
                text: page.eventsource
                font.pixelSize: Theme.fontSizeTiny
                font.family: "monotype"
            }
        }

    }
}
