import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Connections {
        target: clienthandler
        onInitReady: {
            bluepill.myuser_id = userid
            bluepill.mydevice_id = deviceid
            pageStack.replace(Qt.resolvedUrl("DashboardPage.qml"))
        }
    }

    allowedOrientations: Orientation.All

    SilicaFlickable {
        anchors.fill: parent

        contentHeight: parent.height

        Column {
            anchors.centerIn: parent
            anchors.margins:Theme.paddingMedium
            width: parent.width
            Image {
                id: logo
                source: "../../images/matrix_logo.png"
                fillMode: Image.PreserveAspectFit
                anchors.margins: Theme.paddingMedium
                sourceSize.width: Theme.iconSizeExtraLarge
            }
            Label {
                id: statusLabel
                width: parent.width
                wrapMode: Text.WordWrap
                padding: Theme.paddingMedium
                text: qsTr("First sync. This may take some time...")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            BusyIndicator {
                size: BusyIndicatorSize.Large
                anchors.horizontalCenter: parent.horizontalCenter
                running: true
            }
        }
    }
}
