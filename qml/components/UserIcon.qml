import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

Rectangle {
    // anchors.horizontalCenter: parent.horizontalCenter
    width: Theme.iconSizeLarge
    height: Theme.iconSizeLarge
    color: get_u_color()
    property string avatarurl
    property bool idirect
    property string roomname
    property string uid
    anchors.margins: Theme.paddingMedium

    function get_u_color() {
        if (!idirect && avatarurl != "") {
            return "white"
        }
        return u_color(uid)
    }


    border.color: "grey"
    border.width: idirect ? 5 : 0
    radius: width * 0.5

    Image {
        id: avatar
        anchors.centerIn: parent
        width: parent.width - (idirect ? 10 : 0)
        height: parent.height - (idirect ? 10 : 0)
        visible: false
        source: avatarurl
    }
    OpacityMask {
        anchors.fill: avatar
        source: avatar
        visible: avatarurl !== ""
        maskSource: Rectangle {
            width: avatar.width
            height: avatar.height
            radius: width * 0.5
            visible: false // this also needs to be invisible or it will cover up the image
        }
    }
    Label {
        anchors.centerIn: parent
        color: "white"
        text: roomname.charAt(0).toUpperCase()
        font.bold: true
        font.pixelSize: parent.width * 0.8 // Theme.fontSizeExtraLarge
        visible: avatarurl === ""
    }
}
