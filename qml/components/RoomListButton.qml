import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

ListItem {
    id: roomItem
    width: Theme.itemSizeExtraLarge
    // height: Theme.itemSizeLarge
    contentHeight: Theme.itemSizeExtraLarge

    menu: ContextMenu {
        id: contextmenu
        MenuItem {
            id: favmenu
            text: qsTr('Favourite')
            font.bold: priority == "fav"
        }
        MenuItem {
            id: lowmenu
            text: qsTr('Low priority')
            font.bold: priority == "low"
        }
        MenuLabel {
            text: "---"
        }
        MenuItem {
            text: qsTr('Leave')
        }
        MenuItem {
            text: qsTr('Preferences')
        }
    }

    Component.onCompleted: {
        // clienthandler.getUnreadCount(room_id)
    }

    UserIcon {
        id: itemrec
        anchors.horizontalCenter: parent.horizontalCenter
        avatarurl: avatar_url ? avatar_url : ""
        // idirect: direct
        roomname: display_name
        uid: display_name
        width: Theme.iconSizeLarge
        height: Theme.iconSizeLarge

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("Room clicked", display_name)
                pageStack.push(Qt.resolvedUrl("../pages/RoomPage.qml"), { room_id: room_id, room_name: display_name } )
            }
            onPressAndHold: openMenu()
        }
        Image {
            anchors.bottom: itemrec.bottom
            anchors.right: itemrec.right
            source: "image://theme/icon-s-secure"
            visible: encrypted
        }
        SequentialAnimation {
            id: blinkAnim
            running: false
            loops: Animation.Infinite

            NumberAnimation {
                target: itemrec
                property: "opacity"
                duration: 1000
                to: 0.5
            }
            NumberAnimation {
                target: itemrec
                property: "opacity"
                duration: 1000
                to: 1.0
            }
        }
        Rectangle {
            id: countrec
            visible: false
            z: 2
            color: "grey"
            anchors.top: itemrec.top
            anchors.right: itemrec.right
            width: Theme.itemSizeExtraSmall / 2
            height: Theme.itemSizeExtraSmall / 2
            radius: width * 0.5
            Label {
                anchors.centerIn: parent
                id: unread
                font.pixelSize: Theme.fontSizeTiny
                font.bold: true
                text: ""
            }
        }
    }

    Connections {
        target: clienthandler
        onStopTyping: {
            blinkAnim.running = false
            itemrec.opacity = 1.0
        }
        onStartTyping: {
            if (room_id !== roomId || user_id === users[0].user_id) {
                return
            }
            blinkAnim.running = true
        }
        onRoomUnread: {
            if (room_id === roomId) {
                countrec.visible = (count !== 0)
                unread.text = count !== 0 ? count : ""
            }
        }
    }

    Label {
        anchors.horizontalCenter: itemrec.horizontalCenter
        anchors.top: itemrec.bottom
        text: display_name
        width: Theme.itemSizeExtraLarge
        height: Theme.itemSizeMedium
        font.pixelSize: Theme.fontSizeTiny
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        padding: 3
    }
}
