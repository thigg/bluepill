<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="21"/>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="32"/>
        <source>[sticker]</source>
        <translation>[sticker]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="34"/>
        <source>[encrypted message]</source>
        <translation>[versleuteld bericht]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="36"/>
        <source>[message redacted]</source>
        <translation>[bericht geredigeerd]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="38"/>
        <source>[unknown]</source>
        <translation>[onbekend]</translation>
    </message>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="113"/>
        <source>is typing...</source>
        <translation>is aan het typen...</translation>
    </message>
</context>
<context>
    <name>DashboardPage</name>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="36"/>
        <source>Create room</source>
        <translation>Ruimte aanmaken</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="32"/>
        <source>Enter room</source>
        <translation>Ga de kamer binnen</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="39"/>
        <source>StartChat</source>
        <translation>Begin chat</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="42"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="92"/>
        <source>Direct Messages</source>
        <translation>Directe berichten</translation>
    </message>
    <message>
        <location filename="../qml/pages/DashboardPage.qml" line="92"/>
        <source>Rooms</source>
        <translation>Kamers</translation>
    </message>
</context>
<context>
    <name>DeleteDevice</name>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="27"/>
        <source>Delete device</source>
        <translation>Verwijder apparaat</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="30"/>
        <source>Device ID</source>
        <translation>Apparaat ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="45"/>
        <source>Device Name</source>
        <translation>Apparaatnaam</translation>
    </message>
    <message>
        <location filename="../qml/pages/DeleteDevice.qml" line="60"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
</context>
<context>
    <name>DeviceListItem</name>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="17"/>
        <source>Verify with emojis</source>
        <translation>Verifieer met emoji&apos;s</translation>
    </message>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="27"/>
        <source>Text verify</source>
        <translation>Tekst verifiëren</translation>
    </message>
    <message>
        <location filename="../qml/components/DeviceListItem.qml" line="54"/>
        <source>Delete device</source>
        <translation>Verwijder apparaat</translation>
    </message>
</context>
<context>
    <name>EventsListItem</name>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="31"/>
        <source>copy</source>
        <translation>kopiëren</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="37"/>
        <source>event source</source>
        <translation>gebeurtenis bron</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="45"/>
        <source>cite</source>
        <translation>citeren</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="53"/>
        <source>delete</source>
        <translation>verwijderen</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="185"/>
        <source>End to end encryption not implemented</source>
        <translation>End-to-end-versleuteling is niet geïmplementeerd</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="187"/>
        <source>redacted</source>
        <translation>bewerkt</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="190"/>
        <source>left the room</source>
        <translation>heeft de kamer verlaten</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="194"/>
        <source>is now </source>
        <translation>is nu</translation>
    </message>
    <message>
        <location filename="../qml/components/EventsListItem.qml" line="198"/>
        <source>entered the room</source>
        <translation>is de kamer binnengekomen</translation>
    </message>
</context>
<context>
    <name>JoinRoom</name>
    <message>
        <location filename="../qml/pages/JoinRoom.qml" line="29"/>
        <source>Join Room</source>
        <translation>Kamer toetreden</translation>
    </message>
</context>
<context>
    <name>KeyVerificationKey</name>
    <message>
        <location filename="../qml/pages/KeyVerificationKey.qml" line="27"/>
        <source>Compare Icons</source>
        <translation>Vergelijk pictogrammen</translation>
    </message>
</context>
<context>
    <name>KeyVerificationStart</name>
    <message>
        <location filename="../qml/pages/KeyVerificationStart.qml" line="31"/>
        <source>Accept Verification?</source>
        <translation>Verificatie accepteren?</translation>
    </message>
    <message>
        <location filename="../qml/pages/KeyVerificationStart.qml" line="63"/>
        <source>wants to verify this device.</source>
        <translation>wil dit apparaat verifiëren.</translation>
    </message>
</context>
<context>
    <name>LogStatusPage</name>
    <message>
        <location filename="../qml/pages/LogStatusPage.qml" line="39"/>
        <source>First sync. This may take some time...</source>
        <translation>Eerste synchronisatie. Dit kan een tijd duren...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="35"/>
        <source>Login error</source>
        <translation>Fout bij inloggen</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="51"/>
        <location filename="../qml/pages/LoginPage.qml" line="122"/>
        <source>Login</source>
        <translation>Inloggen</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="73"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="90"/>
        <source>Client Name</source>
        <translation>Cliënt Naam</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="97"/>
        <source>Custom Server</source>
        <translation>Aangepaste server</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="102"/>
        <source>Home server URL</source>
        <translation>Thuisserver URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="112"/>
        <source>https://matrix.org</source>
        <translation>https://matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginPage.qml" line="154"/>
        <source>Register</source>
        <translation>Registreren</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="26"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="46"/>
        <source>User settings</source>
        <translation>Gebruikersinstellingen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="53"/>
        <source>Security</source>
        <translation>Beveiliging</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="62"/>
        <source>My Sessions</source>
        <translation>Mijn sessies</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="74"/>
        <source>Messages</source>
        <translation>Berichten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="85"/>
        <source>Font size</source>
        <translation>Lettertypegrootte</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="93"/>
        <source>Markdown rendering</source>
        <translation>Markdown weergave</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="94"/>
        <source>Render messages with Markdown</source>
        <translation>Geef berichten weer met Markdown</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="99"/>
        <source>Notify favourites only</source>
        <translation>Alleen favorieten melden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="100"/>
        <source>Only notify for new messages from favourite rooms</source>
        <translation>Alleen melden bij nieuwe berichten vanuit favoriete kamers</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="110"/>
        <source>Logout</source>
        <translation>Uitloggen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Preferences.qml" line="118"/>
        <source>Logging out</source>
        <translation>Aan het uitloggen</translation>
    </message>
</context>
<context>
    <name>RedactMessage</name>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="17"/>
        <source>Reason for redaction</source>
        <translation>Reden voor bewerking</translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="18"/>
        <source>Reason</source>
        <translation>Reden</translation>
    </message>
    <message>
        <location filename="../qml/pages/RedactMessage.qml" line="24"/>
        <source>Do you really wish to redact (delete) this event? This cannot be undone.</source>
        <translation>Wilt u dit evenement echt bewerken (verwijderen)? Dit kan niet ongedaan gemaakt worden.</translation>
    </message>
</context>
<context>
    <name>RegisterPage</name>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="21"/>
        <location filename="../qml/pages/RegisterPage.qml" line="48"/>
        <source>Register</source>
        <translation>Registreren</translation>
    </message>
    <message>
        <location filename="../qml/pages/RegisterPage.qml" line="42"/>
        <source>Email address (optional)</source>
        <translation>E-mailadres</translation>
    </message>
</context>
<context>
    <name>RoomListItem</name>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="28"/>
        <source>Favourite</source>
        <translation>Favoriet</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="33"/>
        <source>Low priority</source>
        <translation>Lage prioriteit</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="40"/>
        <source>Leave</source>
        <translation>Verlaten</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="42"/>
        <source>leaving room</source>
        <translation>kamer aan het verlaten</translation>
    </message>
    <message>
        <location filename="../qml/components/RoomListItem.qml" line="48"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
</context>
<context>
    <name>RoomPage</name>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="45"/>
        <source>Preferences: </source>
        <translation>Voorkeuren: </translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomPage.qml" line="49"/>
        <source>Preferences</source>
        <translation>Voorkeuren</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="13"/>
        <source>Connected...</source>
        <translation>Verbonden...</translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="16"/>
        <source>Not connected, trying again later...</source>
        <translation>Niet verbonden, later opnieuw proberen...</translation>
    </message>
    <message>
        <location filename="../qml/pages/StartPage.qml" line="44"/>
        <source>Starting engine...</source>
        <translation>Engine starten...</translation>
    </message>
</context>
<context>
    <name>TextControl</name>
    <message>
        <location filename="../qml/components/TextControl.qml" line="57"/>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/TextControl.qml" line="112"/>
        <source> is typing...</source>
        <translation> is aan het typen...</translation>
    </message>
</context>
<context>
    <name>TextVerification</name>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="27"/>
        <source>Verify device</source>
        <translation>Apparaat verifiëren</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="46"/>
        <source>Device ID</source>
        <translation>Apparaat ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="61"/>
        <source>Device Name</source>
        <translation>Apparaatnaam</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="76"/>
        <source>User ID</source>
        <translation>Gebruikers ID</translation>
    </message>
    <message>
        <location filename="../qml/pages/TextVerification.qml" line="91"/>
        <source>Session Key</source>
        <translation>Sessiesleutel</translation>
    </message>
</context>
<context>
    <name>UploadSelector</name>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="17"/>
        <source>Upload...</source>
        <translation>Uploaden...</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="21"/>
        <source>Image</source>
        <translation>Afbeelding</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="25"/>
        <source>Take a picture</source>
        <translation>Afbeelding nemen</translation>
    </message>
    <message>
        <location filename="../qml/pages/UploadSelector.qml" line="98"/>
        <source>Send image</source>
        <translation>Afbeelding versturen</translation>
    </message>
</context>
<context>
    <name>UserSessions</name>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="59"/>
        <source>Login error</source>
        <translation>Fout bij inloggen</translation>
    </message>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="60"/>
        <source>Wrong password</source>
        <translation>Verkeerd wachtwoord</translation>
    </message>
    <message>
        <location filename="../qml/pages/UserSessions.qml" line="69"/>
        <source>Devices of </source>
        <translation>Apparaten van </translation>
    </message>
</context>
<context>
    <name>harbour-bluepill</name>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="60"/>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="46"/>
        <source>never</source>
        <translation>nooit</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="61"/>
        <source>just now</source>
        <translation>zojuist</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="62"/>
        <source>1 minute ago</source>
        <translation>1 minuut geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="63"/>
        <source> minutes ago</source>
        <translation> minuten geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="64"/>
        <source>1 hour ago</source>
        <translation>1 uur geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="65"/>
        <source> hours ago</source>
        <translation> uur geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="68"/>
        <source>Yesterday</source>
        <translation>Gisteren</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="69"/>
        <source> days ago</source>
        <translation> dagen geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="70"/>
        <source> weeks ago</source>
        <translation> weken geleden</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="137"/>
        <source>New posts available</source>
        <translation>Nieuwe berichten beschikbaar</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="138"/>
        <source>Click to view updates</source>
        <translation>Tik om updates te zien</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="139"/>
        <source>New Posts are available. Click to view.</source>
        <translation>Nieuwe berichten beschikbaar. Tik om te bekijken.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-bluepill.qml" line="163"/>
        <source>Room login failed</source>
        <translation>Inloggen misklukt</translation>
    </message>
</context>
</TS>
