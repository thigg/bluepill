#!/bin/bash

if [ -n "$1" ]
then
  TARGETARCH=$1
else
  TARGETARCH='armv7hl'
fi

OLMVERSION=3.2.1
SFVERSION=4.0.1.45

echo "copying source"
mkdir ~nemo/bluepill
rsync -a /source/* ~nemo/bluepill
echo "done"
cd ~nemo/bluepill
mkdir -p build/lib

# get the develop environment

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y python3-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y python3-pip
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y gcc
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y gcc-c++
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y make
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y glib2-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y libffi-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y libjpeg-turbo-devel

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R python3 -m venv ~nemo/bluepill/build/

# Download all needed stuff

git clone https://gitlab.matrix.org/matrix-org/olm.git -b $OLMVERSION
git clone https://github.com/cy8aer/matrix-nio.git

# Create libolm, install in system and copy libolm.so.3 to lib

pushd olm
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R make install
cp build/libolm.so.$OLMVERSION ~nemo/bluepill/build/lib/libolm.so.3
popd

# Other packages
export YARL_NO_EXTENSIONS=1

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install matrix-nio[e2e]==0.17.0
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install pillow
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install emoji
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install markdown2
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install aiofiles
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install -U yarl==1.5.1
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R sed -i "s/, requote=False//g" /home/nemo/bluepill/build/lib/python3.8/site-packages/yarl/_url.py
