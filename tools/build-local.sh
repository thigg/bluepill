#!/bin/bash
TARGETARCH=$1
OLMVERSION=3.1.2
SFVERSION=4.0.1.45

echo "We are in $(pwd)"

# get the develop environment

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y python3-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y python3-pip
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y gcc
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y gcc-c++
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y make
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y glib2-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y libffi-devel
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R zypper install -y libjpeg-turbo-devel

# cp -a /drone/src ~nemo/bluepill
cd ~nemo
mkdir -p build/lib

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R python3 -m venv /home/nemo/bluepill/build/

# update pip

sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install --upgrade pip

# Download all needed stuff

git clone https://gitlab.matrix.org/matrix-org/olm.git -b $OLMVERSION

# Create libolm, install in system and copy libolm.so.3 to lib

pushd olm
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R make install
cp build/libolm.so.$OLMVERSION ~nemo/bluepill/build/lib/libolm.so.3
popd

# Python packages

export YARL_NO_EXTENSIONS=1
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install matrix-nio[e2e]
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install pillow
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install emoji
sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install markdown2
#sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install aiofiles
# sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R ~nemo/bluepill/build/bin/pip install -U yarl==1.5.1
# sb2 -t SailfishOS-$SFVERSION-$TARGETARCH -m sdk-install -R sed -i "s/, requote=False//g" /home/nemo/bluepill/build/lib/python3.8/site-packages/yarl/_url.py

mb2 -t SailfishOS-$SFVERSION-$TARGETARCH build bluepill
