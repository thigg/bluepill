#+TITLE: Todo for rebuilding the data of BluePill
#+AUTHOR: Thomas Renard
#+EMAIL: Thomas.Renard@g3la.de

* TODO client.py
  - [ ] let all variables needed for connectivity in client
  - [X] global listener
  - [ ] make client persistant
  - [ ] signaling to ClientHandler, just generators
  - [ ] make it more functional

* TODO room.py
  - [X] create new persistant class
  - [X] store timeline in room
  - [X] when active: room listener for user experience but fill
    timeline from client global listener
  - [ ] room methods from client
  - [ ] room_name or more information e.g. for events

* TODO user.py
  - really needed?
  - [ ] user_name

